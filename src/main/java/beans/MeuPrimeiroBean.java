package beans;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SessionScoped
@Named
public class MeuPrimeiroBean implements Serializable {
  private static final long serialVersionUID = 1L;
  private String ola = "<strong>Olá amigos.</strong>";
  private boolean exibir = true;
}
