package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
public class GeralBean implements Serializable {
  private Locale locale;
  private List<Locale> localesDisponiveis = new ArrayList<>();

  public GeralBean() {
    Iterator<Locale> it = FacesContext.getCurrentInstance().getApplication().getSupportedLocales();
    while (it.hasNext()) {
      localesDisponiveis.add(it.next());
    }
    locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public List<Locale> getLocalesDisponiveis() {
    return localesDisponiveis;
  }

  public void setLocalesDisponiveis(List<Locale> localesDisponiveis) {
    this.localesDisponiveis = localesDisponiveis;
  }

  public void atualizarLocale(ValueChangeEvent event) {
    FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) event.getNewValue());
  }

  public void atualizarLocale(ActionEvent event) {
    Locale l = (Locale) event.getComponent().getAttributes().get("locale");
    if (l != null) locale = l;
    FacesContext.getCurrentInstance().getViewRoot().setLocale(this.locale);
  }

  public void atualizarLocale() {
    System.out.println("locale atualizado.");
  }
}
