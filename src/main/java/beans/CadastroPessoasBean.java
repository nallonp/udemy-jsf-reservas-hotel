package beans;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import jdk.nashorn.internal.ir.CallNode;
import lombok.Getter;
import lombok.Setter;
import modelo.Pessoa;
import modelo.PessoaFisica;
import modelo.PessoaJuridica;
import modelo.Sexo;

@Getter
@Setter
@ApplicationScoped
@ManagedBean
public class CadastroPessoasBean implements Serializable {
  private static final long serialVersionUID = 1L;
  private Pessoa pessoaSelecionada;
  private List<Pessoa> lista = new ArrayList<>();
  private String tipoNovaPessoa;
  private Locale locale;
  private String codigoGet;

  public CadastroPessoasBean() {
    for (int x = 0; x < 10; x++) {
      Pessoa p = (x % 2 == 0) ? new PessoaFisica() : new PessoaJuridica();
      p.setCodigo(x + 1L);
      p.setNome(String.format("Pessoa %02d", x));
      p.setTelefone(String.format("9999-88%02d", x));
      p.setEmail(String.format("fulano-%02d@email.com", x));
      lista.add(p);
      locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
    }
  }

  public Sexo[] getSexos() {
    return Sexo.values();
  }

  public void criar() {
    FacesContext context = FacesContext.getCurrentInstance();
    if (tipoNovaPessoa == null) {
      context.addMessage(
          null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Você deve especificar o tipo.", ""));
      return;
    } else if (tipoNovaPessoa.equals("PF")) {
      pessoaSelecionada = new PessoaFisica();
    } else if (tipoNovaPessoa.equals("PJ")) {
      pessoaSelecionada = new PessoaJuridica();
    }
    context.addMessage(
        null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pessoa criada com sucesso.", ""));
  }

  public String salvar() {
    if (!lista.contains(this.pessoaSelecionada)) lista.add(this.pessoaSelecionada);
    FacesContext contexto = FacesContext.getCurrentInstance(); /*
    contexto.addMessage(
        null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Edição realizada com sucesso.", ""));*/
    contexto
        .getExternalContext()
        .getFlash()
        .put(
            "mensagem",
            new FacesMessage(FacesMessage.SEVERITY_INFO, "Edição realizada com sucesso.", ""));
    return "sucesso";
  }

  public void excluir() {
    lista.remove(this.pessoaSelecionada);
    pessoaSelecionada = null;
    String mensagem = ResourceBundle.getBundle("bundles.messages", locale).getString("excluida");
    FacesContext.getCurrentInstance()
        .addMessage(
            null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pessoa excluída com sucesso.", ""));
  }

  public String cancelar() {
    pessoaSelecionada = null;
    tipoNovaPessoa = null;
    return "inicio";
  }

  public void setCodigoGet(String codigoGet) {
    this.codigoGet = codigoGet;
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("SETOU O CÓDIGO GET PARA " + codigoGet);
  }

  public boolean isPessoaFisicaSelecionada() {
    return pessoaSelecionada instanceof PessoaFisica;
  }

  public boolean isPessoaJuridicaSelecionada() {
    return pessoaSelecionada instanceof PessoaJuridica;
  }

  public void ouvinteAjax(AjaxBehaviorEvent event) {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("AJAX " + event.getPhaseId());
  }

  public void ouvinteAjax(ValueChangeEvent event) {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("AJAX value change " + event.getPhaseId());
  }

  public void viewListener(ComponentSystemEvent event) {
    if (codigoGet != null && !codigoGet.isEmpty()) {
      pessoaSelecionada = lista.get(Integer.parseInt(getCodigoGet()) - 1);
      Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
          .info("viewListener chamado. Pessoa selecionada: " + getPessoaSelecionada().getNome());
    }
  }
}
