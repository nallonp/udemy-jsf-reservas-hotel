package beans;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import modelo.Quarto;
import modelo.TipoDeQuarto;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@ManagedBean
@SessionScoped
@Getter
@Setter
public class CadastroQuartosBean {
  private Quarto quartoAtual;
  private List<Quarto> lista;
  // CDI não é compatível com Backing Beans
  private HtmlSelectOneMenu selectTipos;

  public CadastroQuartosBean() {
    this.quartoAtual = new Quarto();
    this.lista = new ArrayList<>();
    this.selectTipos = new HtmlSelectOneMenu();
    UISelectItems itens = new UISelectItems();
    itens.getAttributes().put("value", TipoDeQuarto.values());
    selectTipos.getChildren().add(itens);
  }

  @PostConstruct
  public void construir() {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Cadastro de quartos construído!");
  }

  @PreDestroy
  public void destruir() {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Cadastro de quartos destruído!");
  }

  public String salvar() {
    if (!lista.contains(this.quartoAtual)) {
      lista.add(this.quartoAtual);
      this.quartoAtual = new Quarto();
    }
    this.quartoAtual = new Quarto();
    FacesContext contexto = FacesContext.getCurrentInstance();
    contexto.addMessage(
        null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Edição realizada com sucesso.", ""));
    return "sucesso";
  }

  public void excluir() {
    lista.remove(quartoAtual);
    quartoAtual = new Quarto();
    FacesContext.getCurrentInstance()
        .addMessage(
            null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Quarto excluído com sucesso.", ""));
  }

  public String cancelar() {
    this.quartoAtual = new Quarto();
    return "inicio";
  }
}
