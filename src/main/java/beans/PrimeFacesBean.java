package beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.el.MethodExpression;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

@ManagedBean
@SessionScoped
public class PrimeFacesBean {
  private Date data;

  public Date getData() {
    return data;
  }

  public void setData(Date data) {
    this.data = data;
  }

  public void definirData(SelectEvent<Date> event) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    setData(event.getObject());
    FacesContext.getCurrentInstance()
        .addMessage(null, new FacesMessage("Data alterada", "A data agora é " + getData()));
  }
}
