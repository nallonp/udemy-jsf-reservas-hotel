package listeners;

import java.util.logging.Logger;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class OuvinteFases implements PhaseListener {

  @Override
  public void afterPhase(PhaseEvent phaseEvent) {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info(phaseEvent.getPhaseId() + " encerrou!");
  }

  @Override
  public void beforePhase(PhaseEvent phaseEvent) {
    Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info(phaseEvent.getPhaseId() + " iniciou!");
  }

  @Override
  public PhaseId getPhaseId() {
    return null;
  }
}
