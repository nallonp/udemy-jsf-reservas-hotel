package modelo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Endereco implements Serializable {
  private static final long serialVersionUID = 1L;

  private String logradouro;
  private String numero;
  private String complemento;
  private String cep;
  private String bairro;
  private String cidade;
  private String uf;
}
