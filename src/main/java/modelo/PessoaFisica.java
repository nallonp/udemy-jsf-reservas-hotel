package modelo;

import java.util.Collection;
import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("PF")
public class PessoaFisica extends Pessoa {
  private static final long serialVersionUID = 1L;
  private String cpf;
  private String rg;

  @Temporal(TemporalType.DATE)
  private Date dataNascimento;

  @ManyToMany(mappedBy = "hospedes")
  private Collection<Diaria> diarias;

  @Enumerated(value = EnumType.STRING)
  private Sexo sexo;
}
