package modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Quarto implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "gen_quarto")
  @SequenceGenerator(name = "gen_quarto", sequenceName = "quarto_codigo_seq")
  private int codigo;

  private String numero;

  @Enumerated(EnumType.STRING)
  private TipoDeQuarto tipo;

  @OneToMany(mappedBy = "quarto")
  private Collection<Diaria> diarias;
}
