package modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("Avulsa")
public class DiariaAvulsa extends Diaria implements Serializable {
  private static final long serialVersionUID = 1L;

  private BigDecimal value;

  @ManyToOne
  @JoinColumn(name = "cod_cliente")
  private Pessoa cliente;
}
