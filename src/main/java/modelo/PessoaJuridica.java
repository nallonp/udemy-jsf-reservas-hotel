package modelo;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("PJ")
public class PessoaJuridica extends Pessoa implements Serializable {
  private static final long serialVersionUID = 1L;
  private String razaoSocial;
  private String cnpj;
  private String inscricaoEstadual;
  private String inscricaoMunicipal;
}
