package modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Sexo {
  M("Masculino"),
  F("Feminino");

  private String nome;
}
