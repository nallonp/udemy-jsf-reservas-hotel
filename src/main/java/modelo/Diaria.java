package modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(schema = "jsf")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo")
public class Diaria implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "gen_diaria")
  @SequenceGenerator(sequenceName = "diaria_codigo_seq", name = "gen_diaria")
  private Long codigo;

  private LocalDate data;

  @ManyToMany
  @JoinTable(
      name = "hospedagem",
      joinColumns = @JoinColumn(name = "cod_diaria"),
      inverseJoinColumns = @JoinColumn(name = "cod_pessoa"))
  private Collection<PessoaFisica> hospedes;

  @ManyToOne
  @JoinColumn(name = "cod_quarto")
  private Quarto quarto;
}
