package modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo") // descreve o subtipo das tabelas que herdam dessa entidade.
@Table(schema = "jsf")
public abstract class Pessoa implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "gen_pessoa")
  @SequenceGenerator(sequenceName = "pessoa_codigo_seq", name = "gen_pessoa")
  private Long codigo;

  private String nome;
  private String telefone;

  @Column(nullable = false, unique = true)
  private String email;

  @Embedded private Endereco endereco;

  @OneToMany(mappedBy = "cliente")
  private Collection<Reserva> reservas;

  @OneToMany(mappedBy = "cliente")
  private Collection<DiariaAvulsa> diariasAvulsas;
}
